# P1: Multithreading en C++
Oscar Herrero Casado

Fecha 11/12/2019

## Prefacio

Objetivos conseguidos

He podido hacer la base, que se puede ver que funciona en el anterior commit, pero ahora
con los threads no me funciona, por lo que los he quitado para que al menos compile el
archivo.
En el main se tratan todas las entradas del "comando" para que, seg�n el n�mero de
par�metros y sus valores, se les hagan ciertos cambios o se lancen errores en caso de
que fueran valores no esperados.
La practica en si es bastante complicada para el aprendizaje que se lleva hasta el momento,
cuanto menos retadora.

## �ndice

1. Sistema
2. Dise�o e Implementaci�n del Software
3. Metodolog�a y desarrollo de las pruebas realizadas
4. Discusi�n
5. Propuestas optativas

## 1. Sistema

No he realizado benchmark porque no he podido realizar el desarrollo con los threads.


## 2. Dise�o e Implementaci�n del Software

El c�digo est� separado en el main y 4 m�todos, uno de los cuales se encarga de realizar los operaciones
(con otro usado para el xor), otro realiza las funciones necesarias para que funcione el Logger y por
�ltimo otro que es el que ejecutan los hilos para que se realicen las operaciones en paralelo.


## 3. Metodolog�a y desarrollo de las pruebas realizadas

No he podido realizar el benchmark.


## 4. Discusi�n

En este apartado se aportar� una valoraci�n personal de la pr�ctica, en la que se destacar�n aspectos como las dificultades encontradas en su realizaci�n y c�mo se han resuelto.

Tambi�n se responder� a las cuestiones formuladas si las hubiere.