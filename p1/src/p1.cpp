//Oscar Herrero Casado

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <condition_variable>
#include <thread>
#include <time.h>
#include <sys/time.h>

////////////////////////////////////////VARIABLES GLOBALES/////////////////////////////////////////////

int tamanoArray = -1;//Tamano del array, al que se le cambiara el valor con el parametro de entrada
int tipoOperacion = -1;//Tipo de operacion que se realizara, cuyo valor cambiara dependiendo de lo que se haya introducido como parametro
double *array;//Array que contiene los numeros con los que se opera
int numThreads = 0;//Numero de threads que se usaran para las operaciones
double resultado = 0.0;//Resultado final de las operaciones
double resultadoOpera = 0.0;//Auxiliar para las operaciones
std::mutex mutex;
std::condition_variable final;//Condicion que se crea si se ejecuta via LOGGER
std::condition_variable condicionLockHilos;//Condicion para establecer el lock entre hilos
std::thread logger;//Thread del logger
bool readyExe = true;//Booleano que informa si los threads pueden ejecutar
bool loggerFin = false;//Indica cuando ha acabado del logger
int threadId = 0;//Indica el numero de thread que es
double solucionLogger = 0.0;//Le dice al logger los valores de los threads
int threadsAcabados = 0;

#define FEATURE_LOGGER 1

//////////////////////////////////////////////CODIGO////////////////////////////////////////////////////
///////////////////////////////////////DECLARACION DE METODOS////////////////////////////////////////////

double opera(double value1, double value2);
double XOR(double a, double b);
void fLogger(int numThreads, std::condition_variable *condicion);
void usaThreads(int iterThread, double *array, double *valorReturn, int orden);

///////////////////////////////////////////////MAIN///////////////////////////////////////////////////////

int main(int argc, char *argv[]){
  timeval ti, tf;
  double time;
  gettimeofday(&ti, NULL);

  if(argc < 3){//Si no se ha pasado el numero minimo de parametros se lanza error
    printf("Parametros insuficientes, minimo 2\n");//Dos a parte del nombre del comando
    return 1;
  }else{
    tamanoArray = atoi(argv[1]);//Se coge el tamano del array del primer valor del array
    if(tamanoArray <= 0){//Si el tamano del array es 0 o menos se lanza error
      printf("Tamano del array incorrecto, debe ser mayor que 0\n");
      return 1;
    }
  }

  if(strcmp(argv[2], "sum") == 0){//Si el segundo parametro es sum lo indica en la operacion
    tipoOperacion = 0;
  }else if(strcmp(argv[2], "sub") == 0){//Si el segundo parametro es sub lo indica en la operacion
    tipoOperacion = 1;
  }else if(strcmp(argv[2], "xor") == 0){//Si el segundo parametro es xor lo indica en la operacion
    tipoOperacion = 2;
  }else{//Si el segundo parametro no es ninguno de los anteriores se lanza error
    printf("Operacion incorrecta, debe ser una de las siguientes: sum, sub o xor\n");
    return 1;
  }

  array = (double *) (malloc(sizeof(double) * tamanoArray));//Se guarda el espacio que se usara para el array
  for(int i = 0; i < tamanoArray; i++){//Se inicializa el array
    array[i] = (double)i;
  }

  if(argc < 5){//Si solo se pasan 3 argumentos o menos (el primer argumento es el comando)
    if(strcmp(argv[3], "--multi-thread") == 0){//Si el tercer parametro es --multi-thread se lanza el error
      printf("No puedes introducir --multi-thread sin especificar el número de threads\n");
      return 1;
    }else if(strcmp(argv[3], "--single-thread") == 0){//Se ha usado --single-thread
      numThreads = 1;
      printf("La operacion se ejecutara con un solo thread\n\n");
    }else{
      printf("El parametro introducido es incorrecto, los validos son --single-thread o --multi-thread\n");
      return 1;
    }
  //Se ha introducido --single-thread (da igual el parametro) o --multi-thread con el valor de 1
  }else if(strcmp(argv[3], "--single-thread") || (strcmp(argv[3], "--multi-thread") == 0 && atoi(argv[4]) == 1)){
    numThreads = 1;
    printf("La operacion se ejecutara con un solo thread\n\n");
  //Se ha introducido --multi-thread con un numero de threads correcto
  }else if(strcmp(argv[3], "--multi-thread") == 0 && atoi(argv[4]) > 1 && atoi(argv[4]) < 13){
    numThreads = atoi(argv[4]);
    printf("La operacion se ejecutara con %d threads\n\n", numThreads);
  }else{//No se ha introducido un parametro valido
    printf("El parametro introducido es incorrecto, los validos son --single-thread o --multi-thread\n");
    return 1;
  }

  std::thread threads[numThreads];//Se crean los hilos designados
  int parteThread = (int)(tamanoArray/numThreads);//Se divide el numero de operaciones que debe realizar cada thread
  int sobrante = tamanoArray % numThreads;//Se calculan las operaciones sobrantes del reparto
  std::condition_variable *condicionLock;//Condicion para tomar el Mutex

  if(FEATURE_LOGGER == 1){
    condicionLock = &final;
    //logger = std::thread(fLogger, numThreads, condicionLock);
  }else{
    condicionLock = &condicionLockHilos;
  }

  if(sobrante != 0){
    //threads[0] = std::thread(usaThreads, parteThread + sobrante, &array[0], &resultadoOpera, 0);

    for(int i = 1; i < numThreads; i++){
      //threads[i] = std::thread(usaThreads, parteThread, &array[parteThread + sobrante], &resultadoOpera, i);
    }
  }else{
    for(int i = 0; i < numThreads; i++){
      //threads[i] = std::thread(usaThreads, parteThread, &array[parteThread], &resultadoOpera, i);
    }
  }

  if(FEATURE_LOGGER == 1){
    logger.join();
  }

  for(int i = 0; i < numThreads; i++){
    threads[i].join();
  }

  gettimeofday(&tf, NULL);
  time = (tf.tv_sec - ti.tv_sec)*1000 + (tf.tv_usec - ti.tv_usec)/1000.0;
  printf("Se han tardado %f milisegundos\n", time);

  printf("El valor de la operacion es: %f\n", resultado);//Se saca el resultado por pantalla

  return 0;
}

///////////////////////////////////////////METODOS////////////////////////////////////////////////
//Realiza las operaciones con el parametro dado
double opera(double value1, double value2){
  double retorna = 0.0;

  switch (tipoOperacion) {
    case 0:
      retorna = value1 + value2;//Se realiza la operacion de suma
      break;
    case 1:
      retorna = value1 - value2;//Se realiza la operacion de resta
      break;
    case 2:
      retorna = XOR(value1, value2);//Se realiza la operacion de xor
      break;
    default:
      printf("Se ha producido un error durante la operacion");//Si no es ninguna de las anteriores (deberia serlo) se lanza error
      return 1;
  }

  return resultadoOpera;
}

//Operacion xor
double XOR(double a, double b){
  return (a || b) && !(a && b);
}

//
void fLogger(int numThreads, std::condition_variable *condicion){
  int threadsProceso;//Contiene el numero de threads procesados
  int orden[numThreads];//Contiene el orden de los threads
  double resultadosThreads[numThreads]; //Array con el resultado de la ejecucion de cada thread

  for(threadsProceso = 0; threadsProceso < numThreads; threadsProceso++){//Sigue mientras queden threads sin procesar
    std::unique_lock<std::mutex> lock(mutex);//Lockea el mutex

    condicionLockHilos.wait(lock, []{condicionLockHilos.notify_one();
      return(!readyExe);});

    orden[threadsProceso] = threadId;
    resultadosThreads[threadId] = solucionLogger;
    readyExe = true;
    condicionLockHilos.notify_one();
    lock.unlock();
  }

  std::unique_lock<std::mutex> lock2(mutex);

  for(int i = 0; i < numThreads; i++){
    printf("LOGGER: El resultado del thread %d e: %f\n", i, resultadosThreads[i]);
    resultado = opera(resultadosThreads[i], resultadoOpera);
  }

  loggerFin = true;
  (*condicion).notify_all();
  lock2.unlock();
}

void usaThreads(int iterThread, double *array, double *valorReturn, int orden){
  double operaEnThread = 0.0;

  for(int i = 0; i < iterThread; i++){
    operaEnThread = opera(operaEnThread, array[i]);
  }

  std::unique_lock<std::mutex> lock(mutex);
  condicionLockHilos.wait(lock, []{return (readyExe || !FEATURE_LOGGER);});
  solucionLogger = operaEnThread;
  *valorReturn = opera(*valorReturn, operaEnThread);
  resultado = opera(resultado, operaEnThread);
  threadId = orden;
  readyExe = false;
  threadsAcabados++;
  condicionLockHilos.notify_all();
  lock.unlock();
}
