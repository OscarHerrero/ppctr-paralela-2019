# P5: Mandelbrot
Oscar Herrero Casado

Fecha 20/12/2019

## Prefacio

Objetivos conseguidos

Implementaci�n en OpenMP del main.

## �ndice

1. Sistema
2. Dise�o e Implementaci�n del Software
3. Metodolog�a y desarrollo de las pruebas realizadas
4. Discusi�n
5. Propuestas optativas

## 1. Sistema

Intel Core i7-5500U 3.0GHz 	8GB DDR3 L Memory
Bash de Ubuntu sobre Windows 10 (explicado en la pr�ctica 2)


## 2. Dise�o e Implementaci�n del Software

Se envuelve en una zona paralela mediante OpenMP la parte del programa que m�s pesadez tiene (por la cantidad de for). En dicha zona se utilizan las etiquetas de
OpenMP que ya he explicado en pr�cticas anteriores.


## 3. Metodolog�a y desarrollo de las pruebas realizadas

No se han realizado benchmarks


## 4. Discusi�n

Esta pr�ctica es bastante extensa en comparaci�n con las dem�s. Por otra parte, es interesante ver que combina lo aprendido en las pr�cticas anteriores en un solo
programa.

La respuesta a la pregunta 1 del Ejercicio 1 han sido contestada con anterioridad.
Las respuestas pertinentes al Ejercicio 2 han sido contestadas con anterioridad.