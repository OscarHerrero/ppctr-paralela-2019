#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include <sys/time.h>
#include <string.h>

void fgauss (int *, int *, long, long);

int main(int argc, char *argv[]) {

   FILE *in;
   FILE *out;
   int i, j, size, seq = 8;
   int **pixels, **filtered;
   struct timeval ti, tf;//Se declaran las variables para el tiempo inicial y final

   if (argc == 2) seq = atoi (argv[1]);

//   chdir("/tmp");
   in = fopen("movie.in", "rb");
   if (in == NULL) {
      perror("movie.in");
      exit(EXIT_FAILURE);
   }

   #ifdef SEQ
     out = fopen("movie_seq.out", "wb");
     if (out == NULL) {
        perror("movie_seq.out");
        exit(EXIT_FAILURE);
     }
   #endif

   #ifdef OMP
     out = fopen("movie_omp.out", "wb");
     if (out == NULL) {
        perror("movie_omp.out");
        exit(EXIT_FAILURE);
     }
   #endif

   long width, height;

   gettimeofday(&ti, NULL);//Se coge el momento en el que empieza las operaciones

   fread(&width, sizeof(width), 1, in);
   fread(&height, sizeof(height), 1, in);

   fwrite(&width, sizeof(width), 1, out);
   fwrite(&height, sizeof(height), 1, out);

   pixels = (int **) malloc (seq * sizeof (int *));
   filtered = (int **) malloc (seq * sizeof (int *));

   #ifdef SEQ//Compilacion condicional de secuencial
     for (i=0; i<seq; i++)
     {
        pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
        filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
     }
     i = 0;
     do
     {
        size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

        if (size)
        {
           fgauss (pixels[i], filtered[i], height, width);
           fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
        }

     } while (!feof(in));

     for (i=0; i<seq; i++)
     {
        free (pixels[i]);
        free (filtered[i]);
     }
   #endif

   #ifdef OMP//Compilacion condicional de OpenMP
     #pragma omp parallel shared(pixels, filtered, height, width)
     {
       #pragma omp master
       {
         for (i=0; i<seq; i++)
         {
            pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
            filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
         }

         i = 0;

         do{
            size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

            if (size){
               #pragma omp task
               {
                 fgauss (pixels[i], filtered[i], height, width);
                 fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
               }

               #pragma omp taskwait
            }
         } while (!feof(in));

         for (i=0; i<seq; i++)
         {
            #pragma omp task
            {
              free (pixels[i]);
            }
            #pragma omp task
            {
              free (filtered[i]);
            }
         }

         #pragma omp task
         {
           free(pixels);
           free(filtered);
         }
       }
     }
   #endif

   fclose(out);
   fclose(in);

   gettimeofday(&tf, NULL);//Se coge el momento en el que acaban las operaciones

   double resultado = (tf.tv_sec - ti.tv_sec)*1000 + (tf.tv_usec - ti.tv_usec)/1000;//Se saca lo que ha tardado la operacion en milisegundos

   #ifdef SEQ
     printf("La operacion se ha realizado en secuencial\n");
   #endif
   #ifdef OMP
     printf("La operacion se ha realizado en paralelo con OpenMP\n");
   #endif
   printf("La operacion ha tardado %f milisegundos\n", resultado);

   return EXIT_SUCCESS;
}

void fgauss (int *pixels, int *filtered, long height, long width)
{
	int y, x, dx, dy;
	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
	int sum;

	for (x = 0; x < width; x++) {
		for (y = 0; y < height; y++)
		{
			sum = 0;
			for (dx = 0; dx < 5; dx++)
				for (dy = 0; dy < 5; dy++)
					if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < height))
						sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
			filtered[x*height+y] = (int) sum/273;
		}
	}
}
