# P4: VideoTask
Oscar Herrero Casado

Fecha 20/12/2019

## Prefacio

Objetivos conseguidos

La ejecuci�n secuencial funciona (vino de base).
Se ha realizado un compilaci�n condicional para facilitar las pruebas y la correcci�n.
Se ha realizado un MakeFile para facilitar las pruebas y la correcci�n.
El c�digo se ha paralelizado correctamente usando OpenMP.
El tiempo de respuesta conseguido es ligeramente superior a la versi�n secuencial (por diversos motivos que se tratar�n m�s adelante).
Los ficheros generados por la versi�n secuencial y la paralela son id�nticos.
Se realiza la medici�n de los tiempos que se tarda en cada tipo de operacion.
Se han realizado los benchmarks.

## �ndice

1. Sistema
2. Dise�o e Implementaci�n del Software
3. Metodolog�a y desarrollo de las pruebas realizadas
4. Discusi�n
5. Propuestas optativas

## 1. Sistema

Intel Core i7-5500U 3.0GHz 	8GB DDR3 L Memory
Bash de Ubuntu sobre Windows 10 (explicado en la pr�ctica 2)


## 2. Dise�o e Implementaci�n del Software

Sobre el c�digo base se ha realizado una divisi�n para que el c�digo secuancial pueda ser utilizado mediante una compilaci�n condicional, con el objetivo
de copiar dicho c�digo a la parte en la que se va transformar en paralelo para que se puedan realizar ejecuciones del mismo archivo tanto en secuancial
como en paralelo sin tener que modificar el c�digo. Complementariamente se han cambiado los nombres de los ficheros de salida del secuencial a "movie_seq.out"
y del paralelo a "movie_omp.out" para poder utilizar el comando diff para compararlos.
Despu�s de esto se han a�adido las etiquetas de OpenMP para hacer la ejecuci�n paralela empleando, como se especifica en el enunciado, el thread master para
crear otros threads que ejecuten las tasks; teniendo en cuenta que puede haber problemas si no se ejecutan en el mismo orden en el que se ejecutaban en
secuencial (por ejemplo operar sobre un array cuando otro thread est� liberando el espacio). Por esto tambi�n se ha aplicado la etiqueta taskwait para
se cumpla ese orden.
Especificando m�s en lo anterior, las etiquetas de OpenMP que se han utilizado en orden de aparici�n han sido las siguientes:
#pragma omp master: El c�digo comprendido en esta zona se ejecuta �nicamente por el thread principal. Se ha utilizado porque es un requisito de la pr�ctica.
#pragma omp task: El c�digo donde se use esta etiquete ser� ejecutado �nicamente por un thread que cree el thread principal, siguiendo este �ltimo su ejecuci�n.
Es otro de los requisitos de la pr�ctica.
#pragma omp taskwait: Se utiliza para que la ejecuci�n del thread se detenga hasta que las tasks pendientes se hayan ejecutado por completo. Es necesaria para que
operaciones con dependencias de otras anteriores no se empiecen a ejecutar mientras dichas anteriores sigan en proceso.


## 3. Metodolog�a y desarrollo de las pruebas realizadas

El programa se ha lanzado sin introducir ning�n par�metro mediante la l�nea de comandos, lo cual ha devuelto una media de 3854 milisegundos sobre la versi�n
secuencial despu�s de realizar 8 pruebas. El resultado usando OpenMP es el siguiente:
OMP: 3722,625 milisegundos

Con esto se puede ver que la optimizaci�n es min�scula. Esto es debido a que, en el siguiente fragmento de c�digo, tuve que elegir meter el taskwait dentro del bucle,
devolviendo un resultado correcto en el archivo o dejarlo fuera y aumentar la optimizaci�n. Como se puede ver decid� optar por mantener el resultado correcto.
do{
   size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

     if (size){
         #pragma omp task
         {
             fgauss (pixels[i], filtered[i], height, width);
             fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
         }

         #pragma omp taskwait
     }
} while (!feof(in));

Ambas llamadas a fgauss y a fwrite deben ser realizadas por el mismo thread porque en fgauss el valor de filtered cambia, y esta variable es usada despu�s por
fwrite. Por otra parte, si no se pone el taskwait dentro del bucle, en la siguiente iteraci�n del bucle otro thread puede cambiar los valores mientras se est�
ejecutando de antes. Sin embargo, la variable debe ser shared para que todos los threads puedan acceder a ella. Sin el taskwait habr�a problemas en la ejecuci�n y
el resultado no ser�a el esperado al comparar con la versi�n secuencial.
Dicho esto, entiendo cu�l es el problema pero no s� cu�l ser�a la forma de solucionarlo.


## 4. Discusi�n

Todas las preguntas de las pr�cticas se han contestado con anterioridad con expcepci�n de las preguntas 5 y 6.
5. El speedup se calcula dividiendo el tiempo de la versi�n secuancial entre el tiempo de la versi�n paralela: 3854/3722,625=1,0353

En terminos de programaci�n esta pr�ctica ha sido m�s complicada que la anterior, pero, por otra parte, est�s usando una funci�n de la que no entiendes muy bien su
funcionamiento, por lo que realmente no sabes a qu� est�s llamando. Adem�s, el uso de OpenMP no es tan complejo como la creaci�n y uso de threads a mano, aunque esta
pr�ctica ha sido bastante diferente a la anterior.