# P2: Optimizing
Oscar Herrero Casado

Fecha 20/12/2019

## Prefacio

Objetivos conseguidos

La ejecuci�n secuencial funciona (vino de base).
El CLI est� correctamente desarrollado y operativo.
Se usa la compilaci�n condicional correctamente.
El archivo MakeFile sigue los patrones marcados.
Las operaciones se pueden realizar mediante el uso de threads del sistema.
Las operaciones se pueden realizar mediante el uso de threads del sistema via OpenMP.
Se realiza la medici�n de los tiempos que se tarda en cada tipo de operacion.
Los benchmarks se han realizado.

## �ndice

1. Sistema
2. Dise�o e Implementaci�n del Software
3. Metodolog�a y desarrollo de las pruebas realizadas
4. Discusi�n
5. Propuestas optativas

## 1. Sistema

Intel Core i7-5500U 3.0GHz 	8GB DDR3 L Memory
Bash de Ubuntu sobre Windows 10 (m�s adelante se explica porqu�)


## 2. Dise�o e Implementaci�n del Software

A parte del c�digo que ven�a dado de base (secuencial) se han a�adido 3 m�todos m�s para la realizaci�n de la paralelizaci�n, 2 para la paralelizaci�n en
C++ y 1 para la paralelizaci�n mediante OpenMp. Dichos m�todos est�n rodeados por condiciones para realizar la compilaci�n condicional.
As� mismo, dentro del main se han creado diferentes llamadas a estos m�todos para que se acceda a ellos dependiendo de con qu� comando se ha realizado la
compilaci�n.
La parte de paralelizaci�n en C++ se ha realizado usando los threads creados (su n�mero depende de la entrada del comando o 1 por defecto) en dicho m�todo
balance�ndo la carga entre ellos, siendo el sobrante (en el caso de que la divisi�n no pueda ser equitativa) para el primer thread. A estos threads se les
pasa como par�metro para su inicio el m�todo que deben ejecutar (operaParalelo), el par�metro data que contiene informaci�n, el inicio del n�mero de operaciones
que realizar�n (por ejemplo desde el n�mero 500), el final del n�mero de operaciones (por ejemplo hasta el n�mero 999) y el puntero a la variable tmp, en la
cual escribir� cada thread al final. Para esto �ltimo se debe usar una variable local dentro del bucle para que no tengan que esperar los threads todo el
rato a que tmp este libre (su escritura es la regi�n cr�tica); en dicha variable se pondr� el valor que cada thread aplica para tmp y luego se sumar�n en
el tmp mediante un mutex. Finalmente se realizar�n el resto de c�lculos para los que no es necesaria la paralelizaci�n.
En la parte de paralelizaci�n mediante OpenMP se ha usado la etiqueta #pragma omp parallel para denotar la zona paralela, seguida de las cl�usulas
shared(data, value), private(tmp) y num_threads(nThreads), las cuales permiten a las variables "data" y "value" ser compartidas por los diferentes threads,
hacen que se cree una variable "tmp" privada para cada thread y hace uso de un n�mero de threads igual a "nThreads" respectivamente. Como siguiente paso,
se ha hecho uso de la etiqueta #pragma omp for que se usa para hacer que el bucle for que lo sigue se ejecute de forma paralela, siguiendo de la
cl�usula private(i, x), que hace las variables "i" y "x" sean privadas de cada thread. Por �ltimo, se dispone de la etiqueta #pragma omp critical, que crea
una zona donde s�lo podr� entrar un thread a la vez, siendo la m�s sentible a los errores; dicha zona es donde se le da su valor a la variable "tmp" mediante
el uso de la variable "tempo" de cada thread, la cual ha cargado con el valor del thread para juntarlo en "tmp". Una vez hecho esto se realizan el resto de
c�lculos que no necesitan ser paralelizados.
Por �ltimo, se dispone de una medida de tiempo usando variables del tipo "timeval" para conocer el momento en el que se empiezan a lanzar las operaciones y el
momento en el que se acaba, con lo cual se puede saber el tiempo (en este caso en milisegundos) con una sencilla operaci�n como se puede ver a continuaci�n:
double resultado = (tf.tv_sec - ti.tv_sec)*1000 + (tf.tv_usec - ti.tv_usec)/1000;
Como se puede ver, se restan los segundos obtenidos antes de las operaciones a los obtenidos despu�s, sucendiendo lo mismo con los microsegundos y pasando
ambos valores a milisegundos. 


## 3. Metodolog�a y desarrollo de las pruebas realizadas

Los benchmarks se han realizado en una bash de Ubuntu sobre Windows 10 debido a la imposibilidad de instalar Ubuntu en mi PC (por alguna raz�n no pod�a acceder
al panel de boot, por lo que no pude instalar Ubuntu en la partici�n que ten�a creada).
Las iteraciones realizadas han sido 420000000, las cuales devolv�an un tiempo de ejecuci�n de una media de 10283,5 milisegundos sobre la versi�n secuencial
despu�s de realizar 8 pruebas. Los resultados con ese n�mero de iteraciones son los siguientes:
SEQ: 10283,5 milisegundos

CPP: 6924,25 milisegundos con 2 threads
OMP: 6689,125 milisegundos con 2 threads

CPP: 5208,625 milisegundos con 4 threads
OMP: 5207 milisegundos con 4 threads

CPP: 4862,875 milisegundos con 8 threads
OMP: 4940,75 milisegundos con 8 threads

![alt Comparacion de tiempos](tiempos.PNG)

Como se puede apreciar, el uso de los threads, tanto con C++ como con OpenMP reducen mucho el tiempo de respuesta de las operaciones, banj�ndolo hasta m�s de la
mitad con el uso de 8 threads. Por otra parte, entre ambos �mbitos paralelos se puede ver que OpenMP es m�s eficiente cuando se usan menos threads, sin embargo,
con 4 threads est�n a la par, y con 8 es superado por la implementaci�n en CPP.


## 4. Discusi�n

Los problemas m�s importantes que he sufrido durante el transcurso de la pr�ctica son debido mayormente a mi falta de conocimiento, como por ejemplo los
relacionados con la compilaci�n condicional y la creaci�n del Makefile, que pudieron ser abordados despu�s de una intensa b�squeda en internet sobre su
uso y sintaxis.
Otros problemas m�s severos estuvieron relacionados con el uso de threads, tanto en C++ como con OpenMP (este �ltimo en menor medida), los cuales me llevaron
mucho m�s tiempo de solucionar. Los errores que me ocurr�an en ambos casos era derivados del mismo problema, la variable "tmp" era sobrescrita por los diferentes
threads porque la pon�a shared y su valor acababa en 0 si la pon�a como privada una vez que sal�a de la regi�n paralela cuando usaba OpenMP o me tardaba demasiado
la ejecuci�n de las operaciones usando threads (cuantos m�s eran m�s tardaba) cuando se trataba de threads de C++. Sin embargo, ambos problemas tuvieron la misma
soluci�n. En el caso de OpenMP utilic� una variable "tempo" dentro del bucle de la zona paralela que se sumaba en la variable "tmp" una vez fuera del for. En el
caso de C++ ten�a la zona cr�tica dentro del bucle, por lo que, cada vez que se ejecutaba, se ten�a que esperar a que el anterior thread dejara libre la variable
"tmp" para el siguiente, lo que solo empeoraba cuando a�ad�as m�s threads; esto tambi�n se arreglo mediante el uso de otra variable "tempo" para dentro del bucle.
Ambos c�digos se exponen debajo:

OpenMP:
#pragma omp parallel shared(data, value, tmp) private(tempo) num_threads(nThreads)
    {
        #pragma omp for private(i, x)
        for (i=0; i<data.numIts; i=i+1) {
          x = (i+(double)1/(int)2.0) * data.gap;
          tempo = tempo + pow(2,2)/(1+pow(x, 2));
        }

        #pragma omp critical
        {
          tmp += tempo;
        }
    }

C++:
void operaParalelo(Data data, long ini, long fin, double *tmp){
    double x;
    double tempo;
    for (int i = ini; i<fin; i=i+1) {
      x = (i+(double)1/(int)2.0) * data.gap;
      tempo = tempo + pow(2,2)/(1+pow(x, 2));
    }

    {//Zona critica cuando todos los threads escriben en la variable tmp
      std::lock_guard<std::mutex> guard(mutex);
      *tmp = *tmp +tempo;
    }
  }


Preguntas:
1. Como se puede ver en la secci�n 2. Dise�o e Implementaci�n del Software, se explica cu�les han sido las optimizaciones que se han realizado respecto al c�digo
secuencial.
2. Como se puede ver en la secci�n 2. Dise�o e Implementaci�n del Software, se explica c�mo he usado las variables de tipo "timeval" para realizar los benchmarks.
Posteriormente dejar� el c�digo de las dos mediciones que se realizan:
Medici�n antes de las operaciones:
	gettimeofday(&ti, NULL);
Medici�n despu�s de las operaciones:
	gettimeofday(&tf, NULL);

La pr�ctica ha sido complicada, pero no tanto como la primera. Adem�s, al realizarla desde diferentes implementaciones te picaba la curiosidad de cu�l iba a ser
m�s �ptima y en qu� condiciones. Como resumen, una pr�ctica complicada pero entretenida.