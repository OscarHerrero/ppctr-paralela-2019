//Oscar Herrero Casado
//Con 369000000 iteraciones sale mas o menos 10 segundos en secuencial (A modificar despues de usar una maquina Linux)
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include <sys/time.h>
#include <omp.h>
#include <mutex>

#include "p2.hpp"

////////////////////////////////// VARIABLES GLOBALES //////////////////////////
long iteraciones = 0;//Numero de iteraciones
int nThreads = 1;//Threads que se van a utilizar en las operaciones
std::mutex mutex;//Mutex para los threads

////////////////////////////////// DECLARACION DE METODOS //////////////////////
int attach(Data* data, long iterations);
Data parse();
void sequential(Data data);//Metodo que opera en sequiencial
void paralelo(Data data);//Metodo que opera en paralelo
void operaParalelo(Data data, long ini, long fin, double *tmp);//Realiza la operacion a paralelizar
void paraleloOMP(Data data);//Metodo que opera en paralelo con OpenMP

/////////////////////////////////// MAIN ///////////////////////////////////////
int main(int argc,char *argv[]){
  if(argc < 2){//Si no tiene ningun parametro se reporta el error
    printf("Parametros insuficientes, minimo 1\n");
    return 1;
  }else if(argc > 2 && argc <4){//Si tiene 2 parametros
    nThreads = atoi(argv[2]);//Se obtiene el numero de threads que se van a utilizar
  }

  iteraciones = atoi(argv[1]);//Se obtiene el numero de iteraciones que se van a utilizar

  timeval ti, tf;//Se declaran las variables para el tiempo inicial y final

  gettimeofday(&ti, NULL);//Se coge el momento en el que empieza las operaciones

  Data data = parse();

  #ifdef SEQ//Compilacion condicional de secuencial
    sequential(data);
  #endif
  #ifdef CPP//Compilacion condicional de paralelo
    paralelo(data);
  #endif
  #ifdef OMP//Compilacion condicional de paralelo con OpenMP
    paraleloOMP(data);
  #endif

  gettimeofday(&tf, NULL);//Se coge el momento en el que acaban las operaciones

  double resultado = (tf.tv_sec - ti.tv_sec)*1000 + (tf.tv_usec - ti.tv_usec)/1000;//Se saca lo que ha tardado la operacion en milisegundos

  //Dependiendo del tipo de operacion se lanza un mensaje u otro
  #ifdef SEQ
    printf("La operacion se ha realizado en secuencial\n");
  #endif
  #ifdef CPP
    printf("La operacion se ha realizado en paralelo\n");
  #endif
  #ifdef OMP
    printf("La operacion se ha realizado en paralelo con OpenMP\n");
  #endif
  printf("La operacion ha tardado %f milisegundos\n", resultado);

  return 0;
}

////////////////////////////////////// METODOS /////////////////////////////////

int attach(Data* data, long iterations){
  if (iterations <= 0){
    return -1;
  }
  Aggregator* agg = (Aggregator*)malloc(sizeof(Aggregator));
  agg->separator = (double)1/(double)iterations;
  agg->A = 1.5;
  agg->B = 2.4;
  agg->C = 1.0;
  data->agg = agg;
  return 0;
}

Data parse(){
  // parse 'long' and set numIts
  long numIts = iteraciones;
  Data d;
  d.numIts = numIts;
  d.gap = 1.0/(double)d.numIts;
  d.value = 0.0;
  if (attach(&d, numIts) != 0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
  return d;
}

/////////////////////////////////// SECUENCIAL /////////////////////////////////
#ifdef SEQ//Compilacion condicional de secuencial
  void sequential(Data data){
  	int i;
    int jump = 2.4;
  	double value;
    double tmp = 0;
    double ellipse;

    int id;
    double x;
    for (i=0; i<data.numIts; i=i+1) {
      x = (i+(double)1/(int)2.0) * data.gap;
      tmp = tmp + pow(2,2)/(1+pow(x, 2));
    }

    value = tmp * data.agg->separator;
    ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

  	printf("ellipse: %f \n", ellipse);
  }
#endif

/////////////////////////////////// PARALELO ///////////////////////////////////
#ifdef CPP//Compilacion condicional de paralelo
  void paralelo(Data data){
    std::thread threads[nThreads];//Se crea el array de threads

    int jump = 2.4;
  	double value;
    double tmp = 0;
    double ellipse;

    int id;

    long numIter = iteraciones/nThreads;
    long resto = iteraciones % nThreads;

    //Se arranca el primer thread
    if(resto > 0){
      threads[0] = std::thread(operaParalelo, data, 0, numIter + resto, &tmp);
    }else{
      threads[0] = std::thread(operaParalelo, data, 0, numIter, &tmp);
    }

    //Se arranca el resto de threads
    for(int i = 1; i < nThreads; i++){
      threads[i] = std::thread(operaParalelo, data, i*numIter, (i + 1) * numIter, &tmp);
    }

    //Se espera a todos los threads
    for (int i=0; i<nThreads; i++){
      threads[i].join();
    }

    value = tmp * data.agg->separator;
    ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

  	printf("ellipse: %f \n", ellipse);
  }
#endif

#ifdef CPP
  void operaParalelo(Data data, long ini, long fin, double *tmp){
    double x;
    double tempo;
    for (int i = ini; i<fin; i=i+1) {
      x = (i+(double)1/(int)2.0) * data.gap;
      tempo = tempo + pow(2,2)/(1+pow(x, 2));
    }

    {//Zona critica cuando todos los threads escriben en la variable tmp
      std::lock_guard<std::mutex> guard(mutex);
      *tmp = *tmp +tempo;
    }
  }
#endif

////////////////////////////////// PARALELO OMP ////////////////////////////////
#ifdef OMP//Compilacion condicional de paralelo en OpenMP
  void paraleloOMP(Data data){
  	int i;
    int jump = 2.4;
  	double value;
    double tmp = 0;
    double tempo = 0;
    double ellipse;

    int id;
    double x;

    #pragma omp parallel shared(data, value, tmp) private(tempo) num_threads(nThreads)
    {
        #pragma omp for private(i, x)
        for (i=0; i<data.numIts; i=i+1) {
          x = (i+(double)1/(int)2.0) * data.gap;
          tempo = tempo + pow(2,2)/(1+pow(x, 2));
        }

        #pragma omp critical
        {
          tmp += tempo;
        }
    }

    value = tmp * data.agg->separator;
    ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

  	printf("ellipse: %f \n", ellipse);
  }
#endif
